FROM openjdk:17-alpine

RUN mkdir "app"
WORKDIR /app

RUN mkdir "logs"
VOLUME $PWD/logs

COPY ./target/fire-rescue-service.jar .

ENV SERVER_PORT=8080
ENV KAFKA_MAIN_TOPIC_NAME="fire-rescue.events"
ENV KAFKA_DLT_TOPIC_NAME="fire-rescue.events-dlt"
ENV KAFKA_BOOTSTRAP_SERVERS="localhost:9092,localhost:9093"
ENV KAFKA_SCHEMA_REGISTRY_URL="http://localhost:8081"
ENV POSTGRES_URL="jdbc:postgresql://localhost:5432/postgres"
ENV POSTGRES_USER="postgres"
ENV POSTGRES_PASSWORD=""

EXPOSE ${SERVER_PORT}

ENTRYPOINT ["java", "-jar"]

CMD ["fire-rescue-service.jar"]