package ams.firerescue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FireRescueApplication {

	public static void main(String[] args) {
		SpringApplication.run(FireRescueApplication.class, args);
	}
}
