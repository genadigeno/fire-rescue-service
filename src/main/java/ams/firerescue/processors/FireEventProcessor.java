package ams.firerescue.processors;

import ams.data.model.FireAccidentModel;
import ams.firerescue.jpa.FireAccident;
import ams.firerescue.jpa.FireAccidentRepository;
import ams.firerescue.mapper.FireRescueMapper;
import ams.firerescue.service.FireAccidentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class FireEventProcessor {
    private final FireAccidentService fireAccidentService;

    public void process(List<ConsumerRecord<String, FireAccidentModel>> records) {
        log.info("total messages - {}", records.size());

        List<FireAccident> batch = new ArrayList<>();
        try {
            for (ConsumerRecord<String, FireAccidentModel> rec : records){
                log.info("Record: value - {}, key - {}", rec.value(), rec.key());
                //collect in a batch
                batch.add(FireRescueMapper.MAPPER.fireAccidentModelToFireAccident(rec.value()));
            }
        } catch (Exception e) {
            log.error("Error processing record", e);
            throw e;//re-throw an exception to trigger the recoverer
        } finally {
            log.info("batch size - {}", batch.size());
            fireAccidentService.saveBatch(batch);//in case an exception still save a batch we got before the exception
            log.info("batch saved");
        }
    }

}
