package ams.firerescue.service;

import ams.firerescue.jpa.FireAccident;
import ams.firerescue.jpa.FireAccidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FireAccidentService {
    private final FireAccidentRepository fireAccidentRepository;

    @Transactional(rollbackFor = Exception.class)
    public void saveBatch(List<FireAccident> batch){
        fireAccidentRepository.saveAll(batch);
    }
}
