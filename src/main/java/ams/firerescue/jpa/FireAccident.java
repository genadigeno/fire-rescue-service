package ams.firerescue.jpa;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Table(name = "fire_accidents", schema = "public")
@Entity
@Getter @Setter
@NoArgsConstructor
public class FireAccident {
    @Id
    @GeneratedValue
    private Long id;
    private String address;
    private String latitude;
    private String longitude;
    private String description;
    @Column(name = "accident_date")
    private LocalDate date;
}
