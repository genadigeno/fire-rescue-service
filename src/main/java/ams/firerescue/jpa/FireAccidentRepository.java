package ams.firerescue.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FireAccidentRepository extends CrudRepository<FireAccident, Long> {
}
